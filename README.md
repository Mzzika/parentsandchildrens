# README #

This is a simple web-page built with underscrore.js library that shows a tree of parents and their childrens.
You may click on the name of the parents to see their childrens.


### Live preview? ###

You may see a live preview of this repo on the following link : http://5.196.17.218/nodes/index.html
