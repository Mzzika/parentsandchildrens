const nodes = [{
    "id": 1,
    "parentsName": "Antonio & Anastazia",
    "childs": ['Jeremy', 'Anthony']
  },
  {
    "id": 2,
    "parentsName": "Jack & Selwa",
    "childs": ['Nick', 'Dorthy', 'Sarah']
  },
  {
    "id": 3,
    "parentsName": "Alex & Jennifer",
    "childs": ['Jojo', 'Marie', 'Laura', 'Lara']
  }
];


$(function() {
  var template = _.template($("#nodes").html());
  $("body").html(template);
});
